# Тестовое задание

Создайте SPA для добавления/редактирования документа.

На стартовом экране отображаются данные пациента (ФИО) и блок с информацией о добавленном документе.
Данный блок должен отображать следующую информацию:
- Наименование типа документа
- Серия, номер и дата выдачи документа
- "Скан копия загружена" - если у документа имеются добавленные файлы. "Скан. копия отсутствует" - при отсутствии файлов.
- Кнопка добавления / редактирования документа

При редактировании открывается модальное окно с формой редактирования.

## Форма
Форма имеет 5 полей:
- **Документ, удостоверяющий личность** - селект с выбором из списка. Список захардкодить (паспорт, загран паспорт итд, любой список)
- **Серия** - инпут
- **Номер**- инпут
- **Дата выдачи** - инпут с дейтпикером с возможностью ручного ввода даты в поле. Формат даты: дд.мм.гггг
- **Скан копии** - отображение (preview) и аплоад файлов. Поддерживаемые форматы файлов - png, jpeg

> Поля "Документ", "Номер" и "Дата выдачи" обязательны для заполнения.

## Реализация
Для реализации используйте:
- **Vue 3** (с использованием Composition API)
- **Typescript**
- **Pinia**
- Библиотеку компонентов **Quasar**
- **Любые другие библиотеки**, которые считаете уместными
- **Дизайн** на Ваше усмотрение

## Обработка данных
Замокать взаимодействие с сервером, используя следующие эндпоинты и форматы:
```
Получение  данных  пациента:
GET  /patient/:patient_id
Response:
{
	"id": "number",
	"first_name": "string",
	"last_name": "string",
	"middle_name": "string",
	"personal_documents": [
		{
			"id":  "number",
			"document_type_id":  "string", // id документа, удостоверяющего личность
			"document_type_name":  "string", // название документа, удостоверяющего личность
			"pd_series":  "string",
			"pd_number":  "string",
			"date_begin":  "string",
			"files": [
				{
					"id":  "number",
					"file_url":  "string",
					"description":  "string",
					"file_extension":  "string"
				}
			]
		}
	]
}

Note: personal_documents - массив, который может содержать несколько доументов. На клиенте брать первый найденый в массиве.
```

```
Создание  персонального  документа
POST  /patients/:patient_id/personal_documents
Request:
{
	"personal_document": {
		"document_type_id":  "number", // id документа, удостоверяющего личность
		"document_type_name":  "string",
		"pd_series":  "string",
		"pd_number":  "string",
		"date_begin":  "string"
	}
}
```
```
Обновление  персонального  документа
PATCH  /personal_documents/:id
Request:
{
	"personal_document": {
		"document_type_id":  "number", // id документа, удостоверяющего личность
		"document_type_name":  "string",
		"pd_series":  "string",
		"pd_number":  "string",
		"date_begin":  "string"
	}
}
```

```
Добавление  файла  в  персональный  документ
POST  /personal_documents/:personal_document_id/files
Request:
FormData
{
	document[file]: File,
	document[description]: "string"
}
```

```
Получение  файла
GET  /files/:id
Ressponse:
{
	"id": "string",
	"documentable_id": "number",
	"file_url": "string",
	"description": "string"
}
```
```
Обновление  файла
PATCH  /files/:id
Request:
FormData
{
	document[file]: File,
	document[description]: "string"
}
```

```
Удаление  файла
DELETE  /documents/:id
```

